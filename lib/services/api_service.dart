import 'package:dio/dio.dart';
import 'package:majootestcase/models/movie_model.dart';

import 'package:majootestcase/services/dio_config_service.dart' as dioConfig;

class ApiServices {
  Future<MovieModel> getMovieList() async {
    try {
      var dio = await dioConfig.dio();
      Response response = await dio.get('');
      MovieModel movieModel = MovieModel.fromJson(response.data);

      return movieModel;
    } catch (e) {
      print(e.toString());
      return null;
    }
  }
}

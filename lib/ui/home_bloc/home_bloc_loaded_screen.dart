import 'package:flutter/material.dart';
import 'package:majootestcase/models/movie_model.dart';
import 'package:majootestcase/ui/home_bloc/home_bloc_detail_screen.dart';
import 'package:shared_preferences/shared_preferences.dart';

import '../../main.dart';

class HomeBlocLoadedScreen extends StatelessWidget {
  final List<Results> data;

  const HomeBlocLoadedScreen({Key key, this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: Text("Movies"),
        actions: <Widget>[
          IconButton(
            icon: Icon(Icons.logout),
            onPressed: () {
              logout(context);
            },
          )
        ],
      ),
      body: ListView.builder(
        padding: EdgeInsets.all(10),
        itemCount: data.length,
        itemBuilder: (context, index) {
          Results result = data[index];
          return GestureDetector(
              onTap: () {
                Navigator.of(context).push(MaterialPageRoute(
                    builder: (_) => HomeBlocDetailScreen(data: result)));
              },
              child: movieItemWidget(result));
        },
      ),
    );
  }

  Widget movieItemWidget(Results data) {
    return Card(
      shape: RoundedRectangleBorder(
          borderRadius: BorderRadius.all(Radius.circular(25.0))),
      child: Column(
        children: [
          Padding(
            padding: EdgeInsets.all(25),
            child: Image.network(
                "https://image.tmdb.org/t/p/w500/" + data.posterPath),
          ),
          Padding(
            padding: EdgeInsets.only(bottom: 25),
            child: Text(data.title ?? data.name,
                textDirection: TextDirection.ltr,
                style: TextStyle(fontWeight: FontWeight.bold, fontSize: 18)),
          )
        ],
      ),
    );
  }

  void logout(BuildContext context) async {
    final SharedPreferences prefs = await SharedPreferences.getInstance();
    prefs.remove("is_logged_in");
    prefs.remove("user_value");
    Navigator.of(context)
        .pushReplacement(MaterialPageRoute(builder: (_) => MyApp()));
  }
}

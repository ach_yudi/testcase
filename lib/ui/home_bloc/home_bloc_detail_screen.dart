import 'package:flutter/material.dart';

import '../../models/movie_model.dart';

class HomeBlocDetailScreen extends StatelessWidget {
  final Results data;
  const HomeBlocDetailScreen({Key key, @required this.data}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(title: Text(data.title ?? data.name)),
      body: SingleChildScrollView(
        child: Padding(
          padding: const EdgeInsets.symmetric(horizontal: 15),
          child: Column(
            crossAxisAlignment: CrossAxisAlignment.start,
            children: [
              Padding(
                padding: EdgeInsets.symmetric(horizontal: 10, vertical: 5),
                child: Image.network(
                    "https://image.tmdb.org/t/p/w500/" + data.posterPath),
              ),
              SizedBox(height: 10),
              Row(
                children: [
                  Text(
                    data.title ?? data.name,
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  ),
                  Text(
                    " (${data.releaseDate.substring(0, 4)})",
                    style: TextStyle(fontSize: 18, fontWeight: FontWeight.bold),
                  )
                ],
              ),
              SizedBox(height: 5),
              Text("Rating ${data.voteAverage}"),
              SizedBox(height: 10),
              Text(data.overview ?? ""),
              SizedBox(height: 10),
              Text("Popularity ${data.popularity}"),
              Text("Release Date ${data.releaseDate}"),
              SizedBox(height: 40),
            ],
          ),
        ),
      ),
    );
  }
}

import 'dart:io';

import 'package:path/path.dart';
import 'package:path_provider/path_provider.dart';
import 'package:sqflite/sqflite.dart';

import '../models/user.dart';

class DatabaseHelper {
  DatabaseHelper._privateConstructor();
  static final DatabaseHelper instance = DatabaseHelper._privateConstructor();

  static Database _database;
  Future<Database> get database async => _database ??= await _initDatabase();

  Future<Database> _initDatabase() async {
    Directory documentsDirectory = await getApplicationDocumentsDirectory();
    String path = join(documentsDirectory.path, 'testcase.db');
    return await openDatabase(
      path,
      version: 1,
      onCreate: _onCreate,
    );
  }

  Future _onCreate(Database db, int version) async {
    await db.execute('''
      CREATE TABLE user(
          email TEXT PRIMARY KEY,
          password TEXT,
          username TEXT
      )
      ''');
  }

  Future<int> add(User user) async {
    Database db = await instance.database;
    return await db.insert('user', user.toJson());
  }

  Future<bool> checkUser(String email) async {
    Database db = await instance.database;
    List<Map> maps =
        await db.query('user', where: 'email = ?', whereArgs: [email]);
    if (maps.length > 0) {
      return true;
    }
    return false;
  }

  Future<bool> login(User user) async {
    Database db = await instance.database;
    List<Map> maps = await db.query('user',
        where: 'email = ? and password = ?',
        whereArgs: [user.email, user.email]);
    if (maps.length > 0) {
      return true;
    }
    return false;
  }

  Future<List<User>> getUsers() async {
    Database db = await instance.database;
    var users = await db.query('user', orderBy: 'username');
    List<User> usersList =
        users.isNotEmpty ? users.map((c) => User.fromJson(c)).toList() : [];
    return usersList;
  }
}

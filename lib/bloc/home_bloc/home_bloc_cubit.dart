import 'package:bloc/bloc.dart';
import 'package:equatable/equatable.dart';
import 'package:majootestcase/models/movie_model.dart';

import 'package:majootestcase/services/api_service.dart';
import 'package:majootestcase/utils/connectivity.dart';

part 'home_bloc_state.dart';

class HomeBlocCubit extends Cubit<HomeBlocState> {
  HomeBlocCubit() : super(HomeBlocInitialState());

  void fetchingData() async {
    emit(HomeBlocInitialState());
    ApiServices apiServices = ApiServices();
    emit(HomeBlocLoadingState());
    MovieModel movieResponse = await apiServices.getMovieList();
    if (movieResponse == null) {
      if (!await ConnectivityInternet().checkConnection()) {
        emit(HomeBlocErrorState("No internet"));
      } else {
        emit(HomeBlocErrorState("Error Unknown"));
      }
    } else {
      emit(HomeBlocLoadedState(movieResponse.results));
    }
  }
}
